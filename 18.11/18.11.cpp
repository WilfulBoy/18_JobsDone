#include <iostream>
#include <string>

struct Gamer
{
    std::string Name;
    int Score;
};

int findSmallestPosition(Gamer* ArrayPlayers, int startPosition, int listLength)
{
    int smallestPosition = startPosition;
    for (int i = startPosition + 1; i < listLength; i++)
    {
        if (ArrayPlayers[i].Score < ArrayPlayers[smallestPosition].Score)
        {
            smallestPosition = i;
        }
    }
    return smallestPosition;
}

void selectionSort(Gamer* ArrayPlayers, int listLength) //сортировка array
{
    for (int i = 0; i < listLength; i++)
    {
        int smallestPosition = findSmallestPosition(ArrayPlayers, i, listLength);
        std::swap(ArrayPlayers[i], ArrayPlayers[smallestPosition]);
    }
    return;
}

int main()
{
    int numberOfPlayers;
    std::cout << "Enter number of players: ";
    std::cin >> numberOfPlayers;

    Gamer* players = new Gamer[numberOfPlayers];

    for (int i = 0; i < numberOfPlayers; i++)
    {
        std::cout << "\nEnter player details!\n";
        std::cout << "Name: ";
        std::cin >> players[i].Name;
        std::cout << "Score: ";
        std::cin >> players[i].Score;
    }

    selectionSort(players, numberOfPlayers);

    std::cout << "\nLeaderboard\n\n";

    for (int i = 0; i < numberOfPlayers; i++)
    {
        std::cout << i + 1 << " " << players[i].Name << " " << players[i].Score << "\n";
    }

    delete[] players;

    return 0;
}








//#include <iostream>
//void b();
//
//void a() { b(); };
//
//int sum(int a, int b, int c)
//{
//	return a + b + c;
//}
//
//
//int main()
//{
//	sum(1);
//}



//int sum(int a, int b, int c)
//{
//	return a - b + c;
//}
//int main()
//{
//	int(*pointer)(int, int, int) = sum;
//	std::cout << sum;
//}









//#include <iostream>
//#include <string>
//int main()
//{
//	int x = 10;
//	int* p = &x;
//	std::cout << p << '\n';
//	std::cout << *p << '\n';
//	double a = 5.2;
//
//	void* p1;
//	p1 = &x;
//	std::cout << *(static_cast<std::string*>(p1));
//}

#include <iostream>

//int main()
//{
//	int a[3] = { 0,1,2 };
//	int* p = &a[0];
//	std::cout << *a << '\n';
//	std::cout << *(a + 1) << '\n';
//	for (int* p = &a[0]; p < a + 4; p++)
//	{
//	std::cout << *p;
//	}
//
//}



//int main()
//{
//	int x;
//	std::cin >> x;
//	int* p = new int[x];
//	delete p;
//	p = nullptr;
//}
// 
//int main()
//{
//	const int x = 1;
//	int* p;
//	int a = 110;
//	p = &a;
//	*p = 10;
//
//
//}



//class Test
//{
//public:
//	int GetX()
//	{
//		return x;
//	}
//private:
//	int x;
//};
//
//void f(int& x)
//{
//	x = x * x;
//
//}
//
//int main()
//{
//	int a = 10;
//	f(a);
//	std::cout << a;
//
//}




//int main()
//{
//	int a[7]{ 0, 1, 2, 3, 4, 5, 6 };
//	for (const auto& element : a)
//	{
//		std::cout << element;
//	}
//
//}

//int main()
//{
//	Test* p = new Test;
//	p->GetX();	
//}

//int main()
//{
//	int x = 5;
//	const int& ref = x;
//	int y = 10;
//	ref = y;
//	std::cout << x << '\n';
//	std::cout << ref;
//}